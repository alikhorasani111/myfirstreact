import Style from './App.module.css';
import React, {useState} from 'react';
import ProductsList from "./component/productList/ProductsList";
import Nav from "./component/navbar/Nav";

function App() {
    const [products, setProducts] = useState(
        [
            {title: "shoes", price: "1000$", quantity: 1, id: 1},
            {title: "cup", price: "2000$", quantity: 2, id: 2},
            {title: "bag", price: "3000$", quantity: 3, id: 3}
        ]
    );

    const changHandler = (e, id) => {
        const index = products.findIndex((p) => p.id === id);
        const product = {...products[index]};
        product.title = e.target.value;

        const cloneProducts = [...products];

        cloneProducts[index] = product;
        setProducts(cloneProducts);
    };
    const incrementHandler = (id) => {
        const index = products.findIndex((p) => p.id === id);
        const cloneProduct = {...products[index]};

        cloneProduct.quantity++;

        const cloneProducts = [...products];
        cloneProducts[index] = cloneProduct;

        setProducts(cloneProducts);
        console.log(cloneProducts);
    };
    const decrementHandler = (id) => {
        const index = products.findIndex((p) => p.id === id);
        const cloneProduct = {...products[index]};

        if (cloneProduct.quantity === 1) {
            const filterProducts = products.filter((p) => p.id !== id);
            setProducts(filterProducts)
        } else {
            cloneProduct.quantity--;
            const cloneProducts = [...products];
            cloneProducts[index] = cloneProduct;
            setProducts(cloneProducts);
        }

    };
    return (
        <>
            <Nav number={products.filter((p) => p.quantity >= 1).length}/>
            <div className={Style.container}>
                <ProductsList
                    products={products}
                    change={changHandler}
                    increment={incrementHandler}
                    decrement={decrementHandler}
                />
            </div>
        </>
    );
}

export default App;
