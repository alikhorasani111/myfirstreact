import Product from "../product/Product";
import React from "react";

const ProductsList = ({products, change, increment, decrement}) => {
    if (products.length === 0) {
        return <h1>There is no product in carts</h1>
    }
    return (
        <div>
            {products.map((product) => {
                return (
                    <Product
                        product={product}
                        key={product.id}
                        changeInput={(e) => change(e, product.id)}
                        incrementPlus={() => increment(product.id)}
                        decrement={() => decrement(product.id)}
                    />
                )
            })}
        </div>
    )
};
export default ProductsList;