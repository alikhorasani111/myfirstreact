import Style from './Style.module.css'

const Nav = ({number}) => {

    return (
        <div className={Style.bg_nav}>
            <h5>My Carts is : { number }</h5>
        </div>
    )
};
export default Nav;