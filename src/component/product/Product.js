import Style from './style.module.css'
import {FaPlus,FaMinus,FaTrashAlt} from 'react-icons/fa'

const Product = ({product, changeInput, incrementPlus,decrement}) => {
    return (
        <div className={Style.box__product}>
            <div className={Style.box__product_flex}>
                <div className={Style.form_number}>
                    <h2>Product Name :{product.title} </h2>
                    <input type="text" onChange={changeInput} placeholder="Change Product Name"/>
                </div>
                <div className={Style.w_100}>
                    <h4 className={Style.quantity}>Product number :
                        <span>{product.quantity}</span>
                    </h4>
                    <div className={Style.quantity}>
                        <button className={`${Style.btn} ${Style.btn_succsess}`} onClick={incrementPlus}><FaPlus
                            color={"green"} size={20}/></button>

                        <button className={product.quantity > 1 ? `${Style.btn} ${Style.btn_outline_minus}`:`${Style.btn} ${Style.btn_minus}`} onClick={decrement}>
                            {product.quantity > 1 ?<FaMinus color={"red"} size={20} /> : <FaTrashAlt  size={20}/> }
                        </button>
                    </div>
                </div>
                <div className={Style.quantity}>
                    <h3>price :</h3>
                    <strong>{product.price}</strong>
                </div>
            </div>
        </div>
    )
};
export default Product